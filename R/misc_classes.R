#

setClass(
  Class = "Coord",
  slots = c(xy = "numeric"))

setValidity(
  Class = "Coord",
  method = function(object) {
    if(length(object@xy) == 2){
      return(TRUE)
    }else{
      return("xy must be a vector of length 2.")
    }
    }
  )

setMethod(
  f = "show",
  signature = "Coord",
  definition = function(object){
    cat(paste(c("x:", "y:"), object@xy, collapse = ", "), fill = T)
  })

Coord <- function(x, y){
  new(Class = "Coord", xy = c(x, y))
}



