# ================================== Path Response =============================

setClass(
  Class = "PathResponse",
  slots = c(
    n = "numeric",
    fs = "numeric",
    frequency_response = "complex"))



#'@export
PathResponse <- function (x, ...) {
  UseMethod("PathResponse", x)
}

#' @rdname PathResponse
#' @name PathResponse
#' @title PathResponse
#'
#' @description
#' A generic method for generating frequency responses for a single eigenray (SnapPath), or across all eigenrays (SnapSource).
#'
#' @param x PathResponse object.
#' @param y PathResponse object.
#' @param wg A Waveguide object.
#' @param n Number of samples for frequency/impulse response.
#' @param fs Sample rate (Hz).
#' @param type 'pressure' or 'particle', for either sound pressure of particle motion.
#' This will affect the phase changes resulting from surface/sediment bounces.
#'
#' @family propagation-modelling
#' @export
#' @example /inst/examples/example_1.R
#' @usage
#' # x is a SnapPath object
#' pathResponse(x, wg, n, fs, type, freq)
PathResponse.SnapPath <- function(x, wg, n, fs, type){

  # Current propagation model is frequency insensitive
  freq = 1

  # ------ Calculate time delay
  arrival_time <- x@path_length / wg@c_1

  # ------ Get reflection coefficient for sediment
  R = reflection_coefficient(
    rho_1 = wg@rho_1, rho_2 = wg@rho_2,
    c_1 = wg@c_1, c_2 = wg@c_2,
    alpha = wg@alpha_p,
    theta = x@angle_grazing,
    omega = freq * pi * 2)

  # ------ Calculate R phase shift
  phase_shift <- -Arg(R)*x@reflections_ground

  # ------ Add phase inversions
  tbl <- c("pressure", "particle")
  match <- pmatch(type, table = tbl)
  if(is.na(match)){
    stop("Argument 'type' must partially match one of the following options: ", paste(tbl, collapse = ", "), ".")
  }else if(match == 1){
    # Sound pressure (invert on surface bounces)
    phase_shift = phase_shift + pi*x@reflections_surface
  }else{
    # Particle motion (invert on sediment bounces)
    phase_shift = phase_shift + pi*x@reflections_ground
  }

  # ------ Calculate attenuation
  if(x@reflections_ground == 0){
    TL = 0
  }else{
    # Sediment bounces
    TL <- -20*log10(x@reflections_ground * abs(R))
  }
  # Spherical spreading
  TL <- TL + 20*log10(x@path_length)

  # ------ Make frequency response
  f <- fft_freq(n = n, fs = fs)
  # init with attenuation scaling and no phase info
  rl <- rep.int(10^(-TL/20), times = n)
  # Half magnitude of symmetric values
  # rl[f$symmetric] <- rl[f$symmetric]/2
  rl[1] <- 0  # set DC to 0

  impulse_freq <- complex(
    real = rl,
    imaginary = 0)

  # ------ Add arrival time delay
  tmp <- f$omega*arrival_time + phase_shift

  # ------ Add near field phase + magnitude adjustment
  # This only applies to particle motion
  # There should be a pi/2 phase shift and exponentially higher magnitude
  # near the point source for PM
  match <- pmatch(type, table = tbl)
  if(match == 2){
    k = f$omega / wg@c_1
    dist <- x@path_length
    # ------ Particle moiton near-field effects
    pm_near <- (1/(wg@rho_1*wg@c_1)) * (1 + (complex(imaginary = 1)/(k*dist)))
    pm_near_mag <- abs(pm_near)
    pm_near_phase <- Arg(pm_near)

    impulse_freq[f$symmetric] <- impulse_freq[f$symmetric] * pm_near_mag[f$symmetric]
    impulse_freq[f$symmetric] <- impulse_freq[f$symmetric] * exp(complex(real = 0, imaginary = -pm_near_phase[f$symmetric]))
  }

  # ------ flip phases on oppose side of spectrum
  # Make the spectrum conjugate symmetric
  tmp[f$side == 2] = -tmp[f$side == 2]
  tmp[!f$symmetric] <- 0
  impulse_freq <- impulse_freq * exp(complex(real = 0, imaginary = -tmp))

  out <- new(
    Class = "PathResponse",
    n = n, fs = fs,
    frequency_response = impulse_freq)

  return(out)

}

#' @name PathResponse
#' @export
#' @usage
#' # x is a SnapSource object
#' pathResponse(x, wg, n, fs, type)
PathResponse.SnapSource <- function(x, wg, n, fs, type){

  for(i in 1:length(x@paths)){
    if(i == 1){
      out <- PathResponse.SnapPath(x@paths[[i]], wg, n, fs, type)
    }else{
      out <- out + PathResponse.SnapPath(x@paths[[i]], wg, n, fs, type)
    }
  }

  return(out)
}



#' @name PathResponse
#' @export
plot.PathResponse <- function(x, ...){

  par(mfrow = c(2,2))

  tmp <- fft_freq(n = x@n, fs = x@fs)
  idx <- which(tmp$side == 1)
  y_mag <- abs(x@frequency_response[idx])
  idx_2 <- which(tmp$side == 1 & tmp$symmetric)
  y_mag[idx_2] <- y_mag[idx_2] * 2

  plot(tmp$omega[idx]/(2*pi), y_mag, t = 'l',
       xlab = "Frequency (Hz)", ylab = "Magnitude |X|")
  plot(tmp$omega[idx]/(2*pi), Arg(x@frequency_response[idx])*180/pi, t = 'l',
       xlab = "Frequency (Hz)", ylab = "Phase Arg(X)", ylim = c(-180, 180))
  abline(h = c(-180, -90, 0 ,90 ,180), lt = 2, col = 'gray')

  par(mfrow = c(2,1), mfg = c(2,1))
  plot(
    x = (0:(x@n - 1))/x@fs,
    y = Re(impulse_response(x)), t = 'l',
       xlab = "Time (s)", ylab = "Impulse Response")

  par(mfrow = c(1,1))


}


#' Generate an impulse-response
#'
#' Transform a frequency response (PathResponse) into the time domain.
#'
#' @param response
#'
#' @family [propagation-modelling]
#' @export
#' @example /inst/examples/example_1.R
impulse_response <- function(response){

  if(!is(response, "PathResponse")){
    stop("Argument 'response' must be a PathResponse object.")
  }

  impulse <- fft(response@frequency_response, inverse = T) / response@n
  return(impulse)

}

#' @name PathResponse
#' @export
`*.PathResponse` <- function(x, y){
  if(!(is(x, "PathResponse") & is(y, "PathResponse"))){
    stop("Both 'x' and 'y' must be path response objects.")
  }

  if(x@fs != y@fs | x@n != y@n){
    stop("'x' and 'y' must have the same number of samples and sampling rate.")
  }

  # Convolve impulse responses (i.e. multiply their frequency domains)
  x@frequency_response <- x@frequency_response * y@frequency_response

  return(x)
}

#' @name PathResponse
#' @export
`+.PathResponse` <- function(x, y){
  if(!(is(x, "PathResponse") & is(y, "PathResponse"))){
    stop("Both 'x' and 'y' must be path response objects.")
  }

  if(x@fs != y@fs | x@n != y@n){
    stop("'x' and 'y' must have the same number of samples and sampling rate.")
  }

  # Convolve impulse responses (i.e. multiply their frequency domains)
  x@frequency_response <- x@frequency_response + y@frequency_response

  return(x)
}
