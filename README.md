# Snap Tracer

An R package for simulating shrimp snaps in shallow water.

This package employs a simple raytracing model to calculate eigen rays and frequency responses for snaps.
These can be convolved with source signals to simulate the effects of propagation in the environment.

### Installation

You can install this with devtools directly from this repo:
```r
devtools::install_git(
  'https://gitlab.com/RTbecard/snaptracer.git', 
  force = T, build_vignettes = T)
```

### Usage

A detailed introduction to the package is given in the `intro` vignette.

```r
# Open vignette
require(snaptracer)
vignette("intro", package = "snaptracer")

```
Examples are also given in the help docs for functions.

```r
?SnapSource
```
