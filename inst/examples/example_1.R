# This example will demonstrate how to model the acoustic propagation
# of a single shrimp snap.

# ------ Setup a single SnapSource object
# This describes the geometry of the source (snapping shrimp) and receiver
# (a hydrophone or vector sensor).

# Location of snap in xy plane (on bottom of water column)
coord_snap = Coord(5, 0)
# Depth of receiver (located at x=0, y=0)
z_receiver = 1
# Depth of water column
water_depth = 2
# Generate a series of possible transmission paths for given location
# reflection_order indicates the maximum number of ray reflections
snap_1 <- SnapSource(
  coord_snap = coord_snap,
  z_receiver = z_receiver,
  water_depth = water_depth,
  reflection_order = 10)
# Show snap info
snap_1
plot(snap_1, col = 'red')

# ------ Plot a single eigenray and show summary info
# Eigenrays are the unique propagation paths between the source and receiver.
# The path length, grazing angle, and surface/bottom reflections of an eigenray
# alter the receiver sound by changing the magnitude and phase of the source
# signal

# Show info for a single snap path
path <- paths(snap_1)[[4]]
path
plot(path, col = 'blue')

# ------ Setup waveguide properties
# The water column is modeled as a Pekeris waveguide --- two layers of fluid
# under a vacuum.  Bottom sediment, such as sand, is treated as a fluid with a
# different density and sound speed than water.
#
# Importantly, the Pekeris waveguide assumes a perfectly flat bottom and surface.

# We'll model the bottom layer as medium sand with medium-frequency geoacosutic
# parameters taken from Ainslie (2010), table 4.18.
wg <- Waveguide(
  rho_1 = 1000, rho_2 = 2086,
  c_1 = 1500, c_2 = 1500*1.1978,
  alpha_p = 0.88)
# Plot the reflection coefficient wrt to grazing angle.
# This shows the bottom loss and phase shift expected for each sediment
# reflection.
plot(wg, f = 50)

# ------ Generate frequency response
# The effects of propagation on a signal can be stored as a frequency response
# (which is an impulse response when transformed into the time-domain).
# Each eigenray creates a unique frequency response.  For multiple eigenrays,
# the frequencies responses are summed.  This gives us the expected magnitude
# and phase shift of the received snap.

# Pick a generous sample size, as we have to allow space for arrival time-delays.
n = 1000
fs = 5000
# Generate an frequency response across all eigenrays
response_1 <- PathResponse(
  x = snap_1,
  wg = wg, n = n, fs = fs,
  type = 'pres')
plot(response_1)

# ------ Convolve with a Ricker pulse
# We'll make a simple simulation where our shrimp snap is a Ricker wavelet.
# Using the frequency response in the PathResponse object, we'll simulate what
# this received signal should look like after propagating through the waveguide.
# This simulates the signal received from all summed eigenrays
signal <- genrick(freq = 300, dt = 1/fs, nw = n + 1)
# Scale signal so its SPLzp = 120dB @ 1m (re uPa)
peak <- 10^(120/20)
signal <- signal * (peak / max(signal))
plot(signal, t = 'l')

receiver <- SnapReceiver(response_1, signal)
# Compare the source signal to the received signal.
plot(receiver)

# We can also plot the received signal from a single eigenray.
path_10 <- paths(snap_1)[[10]]
plot(path_10, col = 'magenta')
receiver_10 <- SnapReceiver(
  PathResponse(x = path_10, wg = wg, n = n, fs = fs, type = 'pres'),
  signal)
plot(receiver_10)


# ------ Simulate particle motion and sound pressure measurements
# The above steps can be automated to simulate the sound pressure and particle
# motion measured by a vector sensor.
# Here, the received particle motion signal will be projected onto the axes of
# the x, y, and z axes of the vector sensor.
vec <- SnapVector(
  snap_source = snap_1, wg = wg,
  source_signal = signal, fs = fs)

plot(vec)

